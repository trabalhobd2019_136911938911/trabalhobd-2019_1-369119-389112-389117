<?php require_once("../conecta.php"); ?>
<!DOCTYPE html>
<html>
<head>
	<title>Pontos</title>
	<script type="text/javascript" src="../jquery.js"></script>
</head>
<body>
	<h1>Inserir Ponto Turístico</h1>
	<form action="novo.php" method="post" id="form">

		<input type="hidden" name="id" value="<?php echo $_POST["id"]; ?>">

		<label>Tipo:</label>
		<select id="tipo" name="tipo">
		  <option <?php if(isset($_POST["tipo"]) && $_POST["tipo"] == "museu") { echo 'selected="selected"';} ?> value="museu">Museu</option>
		  <option <?php if(isset($_POST["tipo"]) && $_POST["tipo"] == "casa de show") { echo 'selected="selected"';} ?> value="casa de show">Casa de Show</option>
		  <option <?php if(isset($_POST["tipo"]) && $_POST["tipo"] == "igreja") { echo 'selected="selected"';} ?> value="igreja">Igreja</option>
		</select>

		<label>Nome:</label>
		<input type="text" name="nome" value="<?php if(isset($_POST["nome"])){echo $_POST["nome"];} ?>">

		<label>Descrição:</label>
		<input type="text" name="desc">

		<label>Bairro:</label>
		<input type="text" name="bairro">

		<label>Rua:</label>
		<input type="text" name="rua">
		
		<label>Número:</label>
		<input type="number" name="num">
		
		<label>CEP:</label>
		<input type="number" maxlength="3" pattern="([0-9]{3})" name="cep">

		<?php
			$sql = "SELECT * FROM `Cidade`";
			$result = mysqli_query($con, $sql);
			echo "<label>Cidade:</label>";
			echo '<select id="cidade" name="cidade">';
			while ($linha = mysqli_fetch_array($result, MYSQLI_ASSOC)){
				echo '<option value="'. $linha["idCidade"] .'" >' . $linha["nome"] . '</option>';
			}
			echo '</select>';
		?>

		<div id="add">
			
		</div>

		<script type="text/javascript">
			$(document).ready(function(){
				$("#tipo").change(function(){
					var tipo = $("#tipo").val();

					var igreja = 
						'<label>Data de Fundação:</label><input type="date" name="dataFun"><label>Estilo:</label><input type="text" name="estilo">';
					var museu = 
						'<label>Data de Fundação:</label><input type="date" name="dataFun"><label>Entrada:</label><input type="number" name="valor"><label>Numero de Salas:</label><input type="number" name="qtdsalas">' ;
					var casa =
						'<label>Hora Inicio:</label><input type="time" name="inicio"><label>Dia Fechado:</label><select name="dia-fechado"><option value="Sunday">Domingo</option><option value="Monday">Segunda</option><option value="Tuesday">Terça</option><option value="Wednesday">Quarta</option><option value="Thursday">Quinta</option><option value="Friday">Sexta</option><option value="Saturday">Sábado</option></select>';

					if(tipo == 'museu'){ $("#add").html(museu); }
					if(tipo == 'igreja'){ $("#add").html(igreja);}
					if(tipo == 'casa de show'){	$("#add").html(casa); }
				});

				$("#tel").click(function(){
					var campo = '';
				});
			});
		</script>

		<input type="submit" value="cadastrar" id="cadastrar" name="Cadastrar">
	</form>

</body>
</html>