-- -----------------------------------------------------
-- Table `id8380532_mydb`.`Agencia`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `id8380532_mydb`.`Agencia` ;

CREATE TABLE IF NOT EXISTS `id8380532_mydb`.`Agencia` (
  `idAgencia` INT NOT NULL AUTO_INCREMENT,
  `nome` VARCHAR(45) NULL,
  `end` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`idAgencia`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `id8380532_id8380532_mydb`.`Cidade`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `id8380532_mydb`.`Cidade` ;

CREATE TABLE IF NOT EXISTS `id8380532_mydb`.`Cidade` (
  `idCidade` INT NOT NULL AUTO_INCREMENT,
  `nome` VARCHAR(45) NOT NULL,
  `populacao` INT NULL,
  `estado` VARCHAR(45) NOT NULL,
  `AgenciaId` INT NOT NULL,
  PRIMARY KEY (`idCidade`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `id8380532_mydb`.`Hotel`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `id8380532_mydb`.`Hotel` ;

CREATE TABLE IF NOT EXISTS `id8380532_mydb`.`Hotel` (
  `idHotel` INT NOT NULL AUTO_INCREMENT,
  `nome` VARCHAR(45) NOT NULL,
  `end_rua` VARCHAR(45) NOT NULL,
  `end_num` VARCHAR(45) NOT NULL,
  `end_bairro` VARCHAR(45) NOT NULL,
  `end_cep` CHAR(8) NOT NULL,
  `categoria_estrelas` INT NULL,
  `CidadeId` INT NOT NULL,
  PRIMARY KEY (`idHotel`, `CidadeId`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `id8380532_mydb`.`Quarto`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `id8380532_mydb`.`Quarto` ;

CREATE TABLE IF NOT EXISTS `id8380532_mydb`.`Quarto` (
  `idQuarto` INT NOT NULL AUTO_INCREMENT,
  `tipo` VARCHAR(45) NULL,
  `quantidade` INT NULL,
  `valor_diaria` FLOAT NULL,
  `HotelId` INT NOT NULL,
  PRIMARY KEY (`idQuarto`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `id8380532_mydb`.`Ponto_turistico`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `id8380532_mydb`.`Ponto_turistico` ;

CREATE TABLE IF NOT EXISTS `id8380532_mydb`.`Ponto_turistico` (
  `idPonto_turistico` INT NOT NULL AUTO_INCREMENT,
  `tipo_ponto` VARCHAR(45) NOT NULL,
  `descricao` VARCHAR(45) NOT NULL,
  `end_bairro` VARCHAR(45) NOT NULL,
  `end_rua` VARCHAR(45) NOT NULL,
  `end_num` VARCHAR(45) NOT NULL,
  `end_cep` CHAR(8) NOT NULL,
  `CidadeId` INT NOT NULL,
  PRIMARY KEY (`idPonto_turistico`, `CidadeId`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `id8380532_mydb`.`Casa_de_show`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `id8380532_mydb`.`Casa_de_show` ;

CREATE TABLE IF NOT EXISTS `id8380532_mydb`.`Casa_de_show` (
  `nome` VARCHAR(45) NULL,
  `hora_inicio` CHAR(4) NULL,
  `dia_fechado` VARCHAR(45) NULL,
  `Ponto_turisticoId` INT NOT NULL,
  PRIMARY KEY (`Ponto_turisticoId`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `id8380532_mydb`.`Restaurante`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `id8380532_mydb`.`Restaurante` ;

CREATE TABLE IF NOT EXISTS `id8380532_mydb`.`Restaurante` (
  `idRestaurante` INT NOT NULL AUTO_INCREMENT,
  `HotelId` INT NULL,
  `Casa_de_show_Ponto_turisticoId` INT NULL,
  `nome` VARCHAR(45) NOT NULL,
  `end_bairro` VARCHAR(45) NOT NULL,
  `end_rua` VARCHAR(45) NOT NULL,
  `end_num` VARCHAR(45) NOT NULL,
  `end_cep` CHAR(8) NOT NULL,
  `categoria` VARCHAR(45) NULL,
  `especialidade` VARCHAR(45) NULL,
  `preco_medio` FLOAT NULL,
  `diaFechado` VARCHAR(45) NULL,
  `CidadeId` INT NOT NULL,
  PRIMARY KEY (`idRestaurante`, `CidadeId`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `id8380532_mydb`.`Igreja`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `id8380532_mydb`.`Igreja` ;

CREATE TABLE IF NOT EXISTS `id8380532_mydb`.`Igreja` (
  `data` DATE NULL,
  `estilo` VARCHAR(45) NULL,
  `nome` VARCHAR(45) NULL,
  `Ponto_turisticoId` INT NOT NULL,
  PRIMARY KEY (`Ponto_turisticoId`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `id8380532_mydb`.`Museu`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `id8380532_mydb`.`Museu` ;

CREATE TABLE IF NOT EXISTS `id8380532_mydb`.`Museu` (
  `nome` VARCHAR(45) NULL,
  `data_fund` DATE NULL,
  `entrada` FLOAT NULL,
  `num_salas` INT NULL,
  `Ponto_turisticoId` INT NOT NULL,
  PRIMARY KEY (`Ponto_turisticoId`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `id8380532_mydb`.`Fundador`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `id8380532_mydb`.`Fundador` ;

CREATE TABLE IF NOT EXISTS `id8380532_mydb`.`Fundador` (
  `idfundador` INT NOT NULL AUTO_INCREMENT,
  `nome` VARCHAR(45) NOT NULL,
  `data_nascimento` DATE NOT NULL,
  `data_falecimento` DATE NULL,
  `nacionalidade` VARCHAR(45) NULL,
  `atividade_profissional` VARCHAR(45) NULL,
  PRIMARY KEY (`idfundador`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `id8380532_mydb`.`Museu_tem_Fundador`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `id8380532_mydb`.`Museu_tem_Fundador` ;

CREATE TABLE IF NOT EXISTS `id8380532_mydb`.`Museu_tem_Fundador` (
  `FundadorId` INT NOT NULL,
  `MuseuId` INT NOT NULL,
  PRIMARY KEY (`FundadorId`, `MuseuId`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `id8380532_mydb`.`Cliente`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `id8380532_mydb`.`Cliente` ;

CREATE TABLE IF NOT EXISTS `id8380532_mydb`.`Cliente` (
  `idCliente` INT NOT NULL AUTO_INCREMENT,
  `nome` VARCHAR(45) NOT NULL,
  `cpf` VARCHAR(45) NOT NULL,
  `tel1` VARCHAR(45) NOT NULL,
  `tel2` VARCHAR(45) NULL,
  `AgenciaId` INT NOT NULL,
  PRIMARY KEY (`idCliente`, `AgenciaId`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `id8380532_mydb`.`Telefone_agencia`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `id8380532_mydb`.`Telefone_agencia` ;

CREATE TABLE IF NOT EXISTS `id8380532_mydb`.`Telefone_agencia` (
  `idTelefone_agencia` INT NOT NULL AUTO_INCREMENT,
  `Numero` VARCHAR(45) NOT NULL,
  `AgenciaId` INT NOT NULL,
  PRIMARY KEY (`idTelefone_agencia`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `id8380532_mydb`.`Telefone_ponto_turistico`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `id8380532_mydb`.`Telefone_ponto_turistico` ;

CREATE TABLE IF NOT EXISTS `id8380532_mydb`.`Telefone_ponto_turistico` (
  `idTelefone_ponto_turistico` INT NOT NULL AUTO_INCREMENT,
  `Numero` VARCHAR(45) NOT NULL,
  `Ponto_turisticoId` INT NOT NULL,
  `Ponto_turistico_CidadeId` INT NOT NULL,
  PRIMARY KEY (`idTelefone_ponto_turistico`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `id8380532_mydb`.`Igreja_tem_Fundador`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `id8380532_mydb`.`Igreja_tem_Fundador` ;

CREATE TABLE IF NOT EXISTS `id8380532_mydb`.`Igreja_tem_Fundador` (
  `IgrejasId` INT NOT NULL,
  `FundadorId` INT NOT NULL,
  PRIMARY KEY (`IgrejasId`, `FundadorId`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `id8380532_mydb`.`telefone_hotel`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `id8380532_mydb`.`telefone_hotel` ;

CREATE TABLE IF NOT EXISTS `id8380532_mydb`.`telefone_hotel` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `HotelId` INT NOT NULL,
  `Numero` VARCHAR(45) NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;

USE `id8380532_mydb` ;

-- -----------------------------------------------------
-- Placeholder table for view `id8380532_mydb`.`view1`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `id8380532_mydb`.`view1` (`nome_Cidade` INT, `nome_igreja` INT, `nome_museu` INT, `nome_casa_de_show` INT, `nome_hotel` INT, `nome_restaurante` INT);

-- -----------------------------------------------------
-- Placeholder table for view `id8380532_mydb`.`view2`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `id8380532_mydb`.`view2` (`nome` INT, `tipo` INT, `quantidade_de_quartos` INT, `valor_medio` INT);

-- -----------------------------------------------------
-- View `id8380532_mydb`.`view1`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `id8380532_mydb`.`view1`;
DROP VIEW IF EXISTS `id8380532_mydb`.`view1` ;
USE `id8380532_mydb`;
CREATE  OR REPLACE VIEW `view1` AS select t1.nome_Cidade,t1.nome_igreja,t2.nome_museu,t3.nome_casa_de_show,t4.nome_hotel,t5.nome_restaurante from ((select C.nome as nome_Cidade, I.nome as nome_igreja from Cidade C,Igreja I,Ponto_turistico P where C.idCidade=P.CidadeId and P.IdPonto_turistico = I.Ponto_turisticoId) as t1
left outer join (select C.nome as nome_Cidade, M.nome as nome_museu from Cidade C,Museu M,Ponto_turistico P where C.idCidade=P.CidadeId and P.IdPonto_turistico = M.Ponto_turisticoId) as t2
 on t1.nome_Cidade= t2.nome_Cidade)
 left outer join (select C.nome as nome_Cidade, CS.nome as nome_casa_de_show from Cidade C,Casa_de_show CS,Ponto_turistico P where C.idCidade=P.CidadeId and P.IdPonto_turistico = CS.Ponto_turisticoId) as t3 
 on t1.nome_Cidade = t3.nome_Cidade
left outer join(select C.nome as nome_Cidade, H.nome as nome_hotel from Cidade C,Hotel H where C.idCidade=H.CidadeId) as t4
 on  t1.nome_Cidade = t4.nome_Cidade
 left outer join(select C.nome as nome_Cidade, R.nome as nome_restaurante from Cidade C, Restaurante R where C.idCidade=R.CidadeId) as t5
 on t1.nome_Cidade = t5.nome_Cidade
 
 order by t1.nome_Cidade;

-- -----------------------------------------------------
-- View `id8380532_mydb`.`view2`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `id8380532_mydb`.`view2`;
DROP VIEW IF EXISTS `id8380532_mydb`.`view2` ;
USE `id8380532_mydb`;
CREATE  OR REPLACE VIEW `view2` AS select c.nome,q.tipo,sum(q.quantidade) as quantidade_de_quartos,avg(q.valor_diaria) as valor_medio from Cidade c,Hotel h, Quarto q 
where c.idCidade= h.CidadeId and q.HotelId=idHotel
group by q.tipo;

-- -----------------------------------------------------
-- Data for table `id8380532_mydb`.`Agencia`
-- -----------------------------------------------------
START TRANSACTION;
USE `id8380532_mydb`;
INSERT INTO `id8380532_mydb`.`Agencia` (`idAgencia`, `nome`, `end`) VALUES (1, 'macaxeira', 'rua n sei bairro sei la cidade qualquer uma');
INSERT INTO `id8380532_mydb`.`Agencia` (`idAgencia`, `nome`, `end`) VALUES (2, 'chicatur', 'rua tal bairro ali cidade alguma');
INSERT INTO `id8380532_mydb`.`Agencia` (`idAgencia`, `nome`, `end`) VALUES (3, 'predoviagens', 'rua gonzaga bairro luis cidade aculá');

COMMIT;


-- -----------------------------------------------------
-- Data for table `id8380532_mydb`.`Cidade`
-- -----------------------------------------------------
START TRANSACTION;
USE `id8380532_mydb`;
INSERT INTO `id8380532_mydb`.`Cidade` (`idCidade`, `nome`, `populacao`, `estado`, `AgenciaId`) VALUES (1, 'Sobral', 220000, 'CE', 1);
INSERT INTO `id8380532_mydb`.`Cidade` (`idCidade`, `nome`, `populacao`, `estado`, `AgenciaId`) VALUES (2, 'São Benedito', 50179, 'CE', 2);
INSERT INTO `id8380532_mydb`.`Cidade` (`idCidade`, `nome`, `populacao`, `estado`, `AgenciaId`) VALUES (3, 'Fortaleza', 2643000, 'CE', 1);
INSERT INTO `id8380532_mydb`.`Cidade` (`idCidade`, `nome`, `populacao`, `estado`, `AgenciaId`) VALUES (4, 'Itapajé', 51538, 'CE', 3);

COMMIT;


-- -----------------------------------------------------
-- Data for table `id8380532_mydb`.`Hotel`
-- -----------------------------------------------------
START TRANSACTION;
USE `id8380532_mydb`;
INSERT INTO `id8380532_mydb`.`Hotel` (`idHotel`, `nome`, `end_rua`, `end_num`, `end_bairro`, `end_cep`, `categoria_estrelas`, `CidadeId`) VALUES (1, 'casa pode', 'rua menino grande', '345', 'centro', '65765544', 2, 1);
INSERT INTO `id8380532_mydb`.`Hotel` (`idHotel`, `nome`, `end_rua`, `end_num`, `end_bairro`, `end_cep`, `categoria_estrelas`, `CidadeId`) VALUES (2, 'couro véi', 'av tabajara', '1500', 'cruzeiro', '62370000', 1, 2);
INSERT INTO `id8380532_mydb`.`Hotel` (`idHotel`, `nome`, `end_rua`, `end_num`, `end_bairro`, `end_cep`, `categoria_estrelas`, `CidadeId`) VALUES (3, 'brisa', 'av beira mar', '452', 'meireles', '62010497', 5, 3);
INSERT INTO `id8380532_mydb`.`Hotel` (`idHotel`, `nome`, `end_rua`, `end_num`, `end_bairro`, `end_cep`, `categoria_estrelas`, `CidadeId`) VALUES (4, 'dona dora', 'rua cap miranda', '152', 'centro', '62370000', 1, 2);
INSERT INTO `id8380532_mydb`.`Hotel` (`idHotel`, `nome`, `end_rua`, `end_num`, `end_bairro`, `end_cep`, `categoria_estrelas`, `CidadeId`) VALUES (5, 'preudos', 'rua lapaz', '842', 'centro', '62485148', 2, 4);

COMMIT;


-- -----------------------------------------------------
-- Data for table `id8380532_mydb`.`Quarto`
-- -----------------------------------------------------
START TRANSACTION;
USE `id8380532_mydb`;
INSERT INTO `id8380532_mydb`.`Quarto` (`idQuarto`, `tipo`, `quantidade`, `valor_diaria`, `HotelId`) VALUES (1, 'suite', 4, 300, 1);
INSERT INTO `id8380532_mydb`.`Quarto` (`idQuarto`, `tipo`, `quantidade`, `valor_diaria`, `HotelId`) VALUES (2, 'suite mega', 6, 1200, 3);
INSERT INTO `id8380532_mydb`.`Quarto` (`idQuarto`, `tipo`, `quantidade`, `valor_diaria`, `HotelId`) VALUES (3, 'duplo', 5, 430, 2);
INSERT INTO `id8380532_mydb`.`Quarto` (`idQuarto`, `tipo`, `quantidade`, `valor_diaria`, `HotelId`) VALUES (4, 'simples', 8, 150, 4);
INSERT INTO `id8380532_mydb`.`Quarto` (`idQuarto`, `tipo`, `quantidade`, `valor_diaria`, `HotelId`) VALUES (5, 'simples', 10, 180, 5);

COMMIT;


-- -----------------------------------------------------
-- Data for table `id8380532_mydb`.`Ponto_turistico`
-- -----------------------------------------------------
START TRANSACTION;
USE `id8380532_mydb`;
INSERT INTO `id8380532_mydb`.`Ponto_turistico` (`idPonto_turistico`, `tipo_ponto`, `descricao`, `end_bairro`, `end_rua`, `end_num`, `end_cep`, `CidadeId`) VALUES (1, 'igreja', 'linda e bela', 'centro', 'pivet doido', '157', '19000000', 1);
INSERT INTO `id8380532_mydb`.`Ponto_turistico` (`idPonto_turistico`, `tipo_ponto`, `descricao`, `end_bairro`, `end_rua`, `end_num`, `end_cep`, `CidadeId`) VALUES (2, 'museu', 'antigo pakas', 'centro', 'belchior', '654', '18000000', 1);
INSERT INTO `id8380532_mydb`.`Ponto_turistico` (`idPonto_turistico`, `tipo_ponto`, `descricao`, `end_bairro`, `end_rua`, `end_num`, `end_cep`, `CidadeId`) VALUES (3, 'casa de show', 'bailao mermo', 'centro', 'littre street', '968', '65000000', 1);
INSERT INTO `id8380532_mydb`.`Ponto_turistico` (`idPonto_turistico`, `tipo_ponto`, `descricao`, `end_bairro`, `end_rua`, `end_num`, `end_cep`, `CidadeId`) VALUES (4, 'igreja', 'santuario', 'centro', 'rua nossa senhora', '702', '62370000', 2);
INSERT INTO `id8380532_mydb`.`Ponto_turistico` (`idPonto_turistico`, `tipo_ponto`, `descricao`, `end_bairro`, `end_rua`, `end_num`, `end_cep`, `CidadeId`) VALUES (5, 'casa de show', 'castelo', 'cidade alta', 'av tabajara', '1000', '62300000', 2);
INSERT INTO `id8380532_mydb`.`Ponto_turistico` (`idPonto_turistico`, `tipo_ponto`, `descricao`, `end_bairro`, `end_rua`, `end_num`, `end_cep`, `CidadeId`) VALUES (6, 'igreja', 'catedral', 'centro', 'av eros', '482', '62010000', 3);
INSERT INTO `id8380532_mydb`.`Ponto_turistico` (`idPonto_turistico`, `tipo_ponto`, `descricao`, `end_bairro`, `end_rua`, `end_num`, `end_cep`, `CidadeId`) VALUES (7, 'casa de show', 'altas horas', 'mucuripe', 'rua zinha', '842', '62010000', 3);
INSERT INTO `id8380532_mydb`.`Ponto_turistico` (`idPonto_turistico`, `tipo_ponto`, `descricao`, `end_bairro`, `end_rua`, `end_num`, `end_cep`, `CidadeId`) VALUES (8, 'museu', 'don jose', 'centro', 'rua dos passos', '748', '62010000', 3);
INSERT INTO `id8380532_mydb`.`Ponto_turistico` (`idPonto_turistico`, `tipo_ponto`, `descricao`, `end_bairro`, `end_rua`, `end_num`, `end_cep`, `CidadeId`) VALUES (9, 'casa de show', 'petrusbar', 'cidade nova', 'rua de baixo', '478', '64820000', 4);
INSERT INTO `id8380532_mydb`.`Ponto_turistico` (`idPonto_turistico`, `tipo_ponto`, `descricao`, `end_bairro`, `end_rua`, `end_num`, `end_cep`, `CidadeId`) VALUES (10, 'igreja', 'matriz', 'centro', 'rua de deus', '197', '64820000', 4);

COMMIT;


-- -----------------------------------------------------
-- Data for table `id8380532_mydb`.`Casa_de_show`
-- -----------------------------------------------------
START TRANSACTION;
USE `id8380532_mydb`;
INSERT INTO `id8380532_mydb`.`Casa_de_show` (`nome`, `hora_inicio`, `dia_fechado`, `Ponto_turisticoId`) VALUES ('donna c', '2300', 'Monday', 3);
INSERT INTO `id8380532_mydb`.`Casa_de_show` (`nome`, `hora_inicio`, `dia_fechado`, `Ponto_turisticoId`) VALUES ('castelo clube', '2100', 'Wednesday', 5);
INSERT INTO `id8380532_mydb`.`Casa_de_show` (`nome`, `hora_inicio`, `dia_fechado`, `Ponto_turisticoId`) VALUES ('altas horas bar', '2000', 'Thursday', 7);
INSERT INTO `id8380532_mydb`.`Casa_de_show` (`nome`, `hora_inicio`, `dia_fechado`, `Ponto_turisticoId`) VALUES ('petrusbar bar', '1900', 'Friday', 9);

COMMIT;


-- -----------------------------------------------------
-- Data for table `id8380532_mydb`.`Restaurante`
-- -----------------------------------------------------
START TRANSACTION;
USE `id8380532_mydb`;
INSERT INTO `id8380532_mydb`.`Restaurante` (`idRestaurante`, `HotelId`, `Casa_de_show_Ponto_turisticoId`, `nome`, `end_bairro`, `end_rua`, `end_num`, `end_cep`, `categoria`, `especialidade`, `preco_medio`, `diaFechado`, `CidadeId`) VALUES (1, 1, NULL, 'casa da cumida', 'centro', 'big street', '345', '64000000', 'self-servico', 'bolo de pote', 7, 'Sunday', 1);
INSERT INTO `id8380532_mydb`.`Restaurante` (`idRestaurante`, `HotelId`, `Casa_de_show_Ponto_turisticoId`, `nome`, `end_bairro`, `end_rua`, `end_num`, `end_cep`, `categoria`, `especialidade`, `preco_medio`, `diaFechado`, `CidadeId`) VALUES (2, 4, NULL, 'dona dora restaurante', 'centro', 'rua cap miranda', '153', '62370000', 'a la carte', NULL, 8, NULL, 2);
INSERT INTO `id8380532_mydb`.`Restaurante` (`idRestaurante`, `HotelId`, `Casa_de_show_Ponto_turisticoId`, `nome`, `end_bairro`, `end_rua`, `end_num`, `end_cep`, `categoria`, `especialidade`, `preco_medio`, `diaFechado`, `CidadeId`) VALUES (3, 2, NULL, 'restaurante pousada cruzeiro', 'cruzeiro', 'av tabajara', '1500', '62370000', 'a la carte/self-service', 'feijoada', 10, 'Monday', 2);
INSERT INTO `id8380532_mydb`.`Restaurante` (`idRestaurante`, `HotelId`, `Casa_de_show_Ponto_turisticoId`, `nome`, `end_bairro`, `end_rua`, `end_num`, `end_cep`, `categoria`, `especialidade`, `preco_medio`, `diaFechado`, `CidadeId`) VALUES (4, NULL, 5, 'marisco', 'meireles', 'silva paulet', '420', '62010000', 'self-service', 'pratos com camarão', 15, 'Friday', 3);
INSERT INTO `id8380532_mydb`.`Restaurante` (`idRestaurante`, `HotelId`, `Casa_de_show_Ponto_turisticoId`, `nome`, `end_bairro`, `end_rua`, `end_num`, `end_cep`, `categoria`, `especialidade`, `preco_medio`, `diaFechado`, `CidadeId`) VALUES (5, NULL, 3, 'restaurante e pizzaria o pedro', 'centro', 'rua de baixo', '240', '62485148', 'a la carte', 'massas e pizzas', 8, 'Tuesday', 4);

COMMIT;


-- -----------------------------------------------------
-- Data for table `id8380532_mydb`.`Igreja`
-- -----------------------------------------------------
START TRANSACTION;
USE `id8380532_mydb`;
INSERT INTO `id8380532_mydb`.`Igreja` (`data`, `estilo`, `nome`, `Ponto_turisticoId`) VALUES ('1678-05-05', 'barroca', 'sai de reto satan', 1);
INSERT INTO `id8380532_mydb`.`Igreja` (`data`, `estilo`, `nome`, `Ponto_turisticoId`) VALUES ('2008-10-04', 'moderna', 'santuario de nossa senhora de fatima', 4);
INSERT INTO `id8380532_mydb`.`Igreja` (`data`, `estilo`, `nome`, `Ponto_turisticoId`) VALUES ('1746-12-02', 'gotica', 'catedral metropolitana', 6);
INSERT INTO `id8380532_mydb`.`Igreja` (`data`, `estilo`, `nome`, `Ponto_turisticoId`) VALUES ('1695-04-05', 'barroca', 'igreja matriz de itapajé', 10);

COMMIT;


-- -----------------------------------------------------
-- Data for table `id8380532_mydb`.`Museu`
-- -----------------------------------------------------
START TRANSACTION;
USE `id8380532_mydb`;
INSERT INTO `id8380532_mydb`.`Museu` (`nome`, `data_fund`, `entrada`, `num_salas`, `Ponto_turisticoId`) VALUES ('antigo mesmo', '1200-01-01', 70, 5, 2);
INSERT INTO `id8380532_mydb`.`Museu` (`nome`, `data_fund`, `entrada`, `num_salas`, `Ponto_turisticoId`) VALUES ('don jose', '1357-02-10', 50, 8, 8);

COMMIT;


-- -----------------------------------------------------
-- Data for table `id8380532_mydb`.`Fundador`
-- -----------------------------------------------------
START TRANSACTION;
USE `id8380532_mydb`;
INSERT INTO `id8380532_mydb`.`Fundador` (`idfundador`, `nome`, `data_nascimento`, `data_falecimento`, `nacionalidade`, `atividade_profissional`) VALUES (1, 'Alberto G', '1500-05-05', '1600-05-05', 'ET', 'Viajante do Espaco');
INSERT INTO `id8380532_mydb`.`Fundador` (`idfundador`, `nome`, `data_nascimento`, `data_falecimento`, `nacionalidade`, `atividade_profissional`) VALUES (2, 'Aberto H', '100-05-05', '1089-10-10', 'ET', 'Viajante do Espaco');
INSERT INTO `id8380532_mydb`.`Fundador` (`idfundador`, `nome`, `data_nascimento`, `data_falecimento`, `nacionalidade`, `atividade_profissional`) VALUES (3, 'Antonio', '1952-07-25', NULL, 'Brazileira', 'Padre');
INSERT INTO `id8380532_mydb`.`Fundador` (`idfundador`, `nome`, `data_nascimento`, `data_falecimento`, `nacionalidade`, `atividade_profissional`) VALUES (4, 'Neto', '1963-08-21', NULL, 'Brazileira', 'Bispo');
INSERT INTO `id8380532_mydb`.`Fundador` (`idfundador`, `nome`, `data_nascimento`, `data_falecimento`, `nacionalidade`, `atividade_profissional`) VALUES (5, 'Arroba', '1982-12-12', NULL, 'Inglesa', 'Historiador');

COMMIT;


-- -----------------------------------------------------
-- Data for table `id8380532_mydb`.`Museu_tem_Fundador`
-- -----------------------------------------------------
START TRANSACTION;
USE `id8380532_mydb`;
INSERT INTO `id8380532_mydb`.`Museu_tem_Fundador` (`FundadorId`, `MuseuId`) VALUES (2, 2);
INSERT INTO `id8380532_mydb`.`Museu_tem_Fundador` (`FundadorId`, `MuseuId`) VALUES (5, 8);

COMMIT;


-- -----------------------------------------------------
-- Data for table `id8380532_mydb`.`Cliente`
-- -----------------------------------------------------
START TRANSACTION;
USE `id8380532_mydb`;
INSERT INTO `id8380532_mydb`.`Cliente` (`idCliente`, `nome`, `cpf`, `tel1`, `tel2`, `AgenciaId`) VALUES (1, 'ze', '12312312311', '85992926666', '88993455432', 1);
INSERT INTO `id8380532_mydb`.`Cliente` (`idCliente`, `nome`, `cpf`, `tel1`, `tel2`, `AgenciaId`) VALUES (2, 'pedra', '14562147849', '88996969696', NULL, 3);
INSERT INTO `id8380532_mydb`.`Cliente` (`idCliente`, `nome`, `cpf`, `tel1`, `tel2`, `AgenciaId`) VALUES (3, 'alice', '64851728492', '88992748148', NULL, 2);
INSERT INTO `id8380532_mydb`.`Cliente` (`idCliente`, `nome`, `cpf`, `tel1`, `tel2`, `AgenciaId`) VALUES (4, 'venancio', '68471198475', '88997825481', NULL, 3);

COMMIT;


-- -----------------------------------------------------
-- Data for table `id8380532_mydb`.`Telefone_agencia`
-- -----------------------------------------------------
START TRANSACTION;
USE `id8380532_mydb`;
INSERT INTO `id8380532_mydb`.`Telefone_agencia` (`idTelefone_agencia`, `Numero`, `AgenciaId`) VALUES (1, '8832324567', 1);
INSERT INTO `id8380532_mydb`.`Telefone_agencia` (`idTelefone_agencia`, `Numero`, `AgenciaId`) VALUES (2, '8836269482', 2);
INSERT INTO `id8380532_mydb`.`Telefone_agencia` (`idTelefone_agencia`, `Numero`, `AgenciaId`) VALUES (3, '8532482971', 3);

COMMIT;


-- -----------------------------------------------------
-- Data for table `id8380532_mydb`.`Telefone_ponto_turistico`
-- -----------------------------------------------------
START TRANSACTION;
USE `id8380532_mydb`;
INSERT INTO `id8380532_mydb`.`Telefone_ponto_turistico` (`idTelefone_ponto_turistico`, `Numero`, `Ponto_turisticoId`, `Ponto_turistico_CidadeId`) VALUES (1, '8867567654', 1, 1);
INSERT INTO `id8380532_mydb`.`Telefone_ponto_turistico` (`idTelefone_ponto_turistico`, `Numero`, `Ponto_turisticoId`, `Ponto_turistico_CidadeId`) VALUES (2, '8863251485', 2, 1);
INSERT INTO `id8380532_mydb`.`Telefone_ponto_turistico` (`idTelefone_ponto_turistico`, `Numero`, `Ponto_turisticoId`, `Ponto_turistico_CidadeId`) VALUES (3, '8836244954', 3, 1);
INSERT INTO `id8380532_mydb`.`Telefone_ponto_turistico` (`idTelefone_ponto_turistico`, `Numero`, `Ponto_turisticoId`, `Ponto_turistico_CidadeId`) VALUES (4, '8835149517', 4, 2);
INSERT INTO `id8380532_mydb`.`Telefone_ponto_turistico` (`idTelefone_ponto_turistico`, `Numero`, `Ponto_turisticoId`, `Ponto_turistico_CidadeId`) VALUES (5, '8839471824', 5, 2);
INSERT INTO `id8380532_mydb`.`Telefone_ponto_turistico` (`idTelefone_ponto_turistico`, `Numero`, `Ponto_turisticoId`, `Ponto_turistico_CidadeId`) VALUES (6, '8532481174', 6, 3);
INSERT INTO `id8380532_mydb`.`Telefone_ponto_turistico` (`idTelefone_ponto_turistico`, `Numero`, `Ponto_turisticoId`, `Ponto_turistico_CidadeId`) VALUES (7, '8532947817', 7, 3);
INSERT INTO `id8380532_mydb`.`Telefone_ponto_turistico` (`idTelefone_ponto_turistico`, `Numero`, `Ponto_turisticoId`, `Ponto_turistico_CidadeId`) VALUES (8, '8532847195', 8, 3);
INSERT INTO `id8380532_mydb`.`Telefone_ponto_turistico` (`idTelefone_ponto_turistico`, `Numero`, `Ponto_turisticoId`, `Ponto_turistico_CidadeId`) VALUES (9, '8836271947', 9, 4);
INSERT INTO `id8380532_mydb`.`Telefone_ponto_turistico` (`idTelefone_ponto_turistico`, `Numero`, `Ponto_turisticoId`, `Ponto_turistico_CidadeId`) VALUES (10, '8836849712', 10, 4);

COMMIT;


-- -----------------------------------------------------
-- Data for table `id8380532_mydb`.`Igreja_tem_Fundador`
-- -----------------------------------------------------
START TRANSACTION;
USE `id8380532_mydb`;
INSERT INTO `id8380532_mydb`.`Igreja_tem_Fundador` (`IgrejasId`, `FundadorId`) VALUES (1, 1);
INSERT INTO `id8380532_mydb`.`Igreja_tem_Fundador` (`IgrejasId`, `FundadorId`) VALUES (4, 3);
INSERT INTO `id8380532_mydb`.`Igreja_tem_Fundador` (`IgrejasId`, `FundadorId`) VALUES (6, 4);
INSERT INTO `id8380532_mydb`.`Igreja_tem_Fundador` (`IgrejasId`, `FundadorId`) VALUES (10, 4);

COMMIT;


-- -----------------------------------------------------
-- Data for table `id8380532_mydb`.`telefone_hotel`
-- -----------------------------------------------------
START TRANSACTION;
USE `id8380532_mydb`;
INSERT INTO `id8380532_mydb`.`telefone_hotel` (`id`, `HotelId`, `Numero`) VALUES (1, 1, '8835654567');
INSERT INTO `id8380532_mydb`.`telefone_hotel` (`id`, `HotelId`, `Numero`) VALUES (2, 2, '8836261925');
INSERT INTO `id8380532_mydb`.`telefone_hotel` (`id`, `HotelId`, `Numero`) VALUES (3, 3, '8532958471');
INSERT INTO `id8380532_mydb`.`telefone_hotel` (`id`, `HotelId`, `Numero`) VALUES (4, 4, '8836262874');
INSERT INTO `id8380532_mydb`.`telefone_hotel` (`id`, `HotelId`, `Numero`) VALUES (5, 5, '8532641824');

COMMIT;

USE `id8380532_mydb`;

DELIMITER $$

USE `id8380532_mydb`$$
DROP TRIGGER IF EXISTS `id8380532_mydb`.`Casa_de_show_BEFORE_INSERT` $$
USE `id8380532_mydb`$$
CREATE DEFINER = CURRENT_USER TRIGGER `id8380532_mydb`.`Casa_de_show_BEFORE_INSERT` BEFORE INSERT ON `Casa_de_show` FOR EACH ROW
BEGIN

declare id int;
set id:=(select Max(idPonto_turistico) from Ponto_turistico);
set new.Ponto_turisticoId:=id;


END$$


USE `id8380532_mydb`$$
DROP TRIGGER IF EXISTS `id8380532_mydb`.`Igreja_BEFORE_INSERT` $$
USE `id8380532_mydb`$$
CREATE DEFINER = CURRENT_USER TRIGGER `id8380532_mydb`.`Igreja_BEFORE_INSERT` BEFORE INSERT ON `Igreja` FOR EACH ROW
BEGIN
declare id int;
set id:=(select Max(idPonto_turistico) from Ponto_turistico);
set new.Ponto_turisticoId:=id;


END$$


USE `id8380532_mydb`$$
DROP TRIGGER IF EXISTS `id8380532_mydb`.`Museu_BEFORE_INSERT` $$
USE `id8380532_mydb`$$
CREATE DEFINER = CURRENT_USER TRIGGER `id8380532_mydb`.`Museu_BEFORE_INSERT` BEFORE INSERT ON `Museu` FOR EACH ROW
BEGIN

declare id int;
set id:=(select Max(idPonto_turistico) from Ponto_turistico);
set new.Ponto_turisticoId:=id;

END$$


DELIMITER ;

SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
