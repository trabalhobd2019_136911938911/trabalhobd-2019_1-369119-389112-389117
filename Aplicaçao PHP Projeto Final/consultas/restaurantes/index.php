<?php require_once("../conecta.php"); ?>
<!DOCTYPE html>
<html>
<head>
	<title>Restaurante</title>
</head>
<body>
	<h1>Lista de Retaurantes</h1>
	<?php
		$sql = "SELECT * FROM Restaurante";
		$result = mysqli_query($con, $sql);
		
		echo '<a href="https://pdrgms.000webhostapp.com/restaurantes/form_inserir.php">Inserir Novo Restaurante</a>';
		echo "<table>";
		while ($linha = mysqli_fetch_array($result, MYSQLI_ASSOC)){
			echo "<tr>";
			echo "<td>" . $linha["nome"] . "</td>";
			echo "<td>" . $linha["categoria"] . "</td>";
			echo "<td>" . $linha["especialidade"] . "</td>";
			echo "<td> R$ " . $linha["preco_medio"] . "</td>";
			echo "<td>" . $linha["diaFechado"] . "</td>";
			echo "<td> Hotel: " . $linha["HotelId"] . "</td>";
			echo "<td> Casa de Show: " . $linha["Casa_de_show_Ponto_turisticoId"] . "</td>";
			echo "<td>" . $linha["end_bairro"] . "</td>";
			echo "<td>" . $linha["end_rua"] . "</td>";
			echo "<td>" . $linha["end_num"] . "</td>";
			echo "<td>" . $linha["end_cep"] . "</td>";
			echo "<td>" . $linha["CidadeId"] . "</td>";
			if($_SESSION['nivel'] != 'cliente'){
			echo '<td><form method="post" action="form_inserir.php">
					<input type="hidden" name="id" value="'. $linha["idRestaurante"] .'">
					<input type="hidden" name="nome" value="'. $linha["nome"] .'">
					<input type="hidden" name="categoria"  value="'. $linha["categoria"] .'">
					<input type="hidden" name="especialidade"  value="'. $linha["especialidade"] .'">
					<input type="hidden" name="preco"  value="'. $linha["preco_medio"] .'">
					<input type="hidden" name="diafechado"  value="'. $linha["diaFechado"] .'">
					<input type="hidden" name="hotel"  value="'. $linha["HotelId"] .'">
					<input type="hidden" name="casadeshow"  value="'. $linha["Casa_de_show_Ponto_turisticoId"] .'">
					<input type="hidden" name="bairro"  value="'. $linha["end_bairro"] .'">
					<input type="hidden" name="rua"  value="'. $linha["end_rua"] .'">
					<input type="hidden" name="num"  value="'. $linha["end_num"] .'">
					<input type="hidden" name="cep"  value="'. $linha["end_cep"] .'">
					<input type="hidden" name="cidade"  value="'. $linha["CidadeId"] .'">
					<input type="submit" value="Alterar" id="alterar" name="alterar">
				  </form></td>';
			}
			if($_SESSION['nivel'] != 'cliente'){
				echo '<td><form method="post" action="deleta.php">
						<input type="hidden" name="id" value="'. $linha["idRestaurante"] .'">
						<input type="submit" value="Deletar" id="deletar" name="deletar">
					  </form></td>';
			}
			echo "</tr>";
		}
		echo "</table>"; 
	 ?>
</body>
</html>