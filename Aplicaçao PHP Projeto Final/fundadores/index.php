<?php require_once("../conecta.php"); ?>
<!DOCTYPE html>
<html>
<head>
	<title>Fundadores</title>
</head>
<body>
	<h1>Lista de Fundadores</h1>
	<?php
		$sql = "SELECT * FROM Fundador";
		$result = mysqli_query($con, $sql);

		echo '<a href="https://pdrgms.000webhostapp.com/fundadores/form_inserir.php">Inserir Novo Fundador</a>';
		echo "<table>";
		while ($linha = mysqli_fetch_array($result, MYSQLI_ASSOC)){
			echo "<tr>";
			echo "<td>" . $linha["nome"] . "</td>";
			echo "<td>" . $linha["data_nascimento"] . "</td>";
			echo "<td>" . $linha["data_falecimento"] . "</td>";
			echo "<td>" . $linha["nacionalidade"] . "</td>";
			echo "<td>" . $linha["atividade_profissional"] . "</td>";
			if($_SESSION['nivel'] != 'cliente'){
			echo '<td><form method="post" action="form_inserir.php">
					<input type="hidden" name="id" value="'. $linha["idfundador"] .'">
					<input type="hidden" name="nome" value="'. $linha["nome"] .'">
					<input type="hidden" name="nascimento"  value="'. $linha["data_nascimento"] .'">
					<input type="hidden" name="falecimento"  value="'. $linha["data_falecimento"] .'">
					<input type="hidden" name="nacionalidade"  value="'. $linha["nacionalidade"] .'">
					<input type="hidden" name="atividade"  value="'. $linha["atividade_profissional"] .'">
					<input type="submit" value="Alterar" id="alterar" name="alterar">
				  </form></td>';
			}
			if($_SESSION['nivel'] != 'cliente'){
				echo '<td><form method="post" action="deleta.php">
						<input type="hidden" name="id" value="'. $linha["idfundador"] .'">
						<input type="submit" value="Deletar" id="deletar" name="deletar">
					  </form></td>';
			}
			echo "</tr>";
		}
		echo "</table>"; 
	 ?>
</body>
</html>