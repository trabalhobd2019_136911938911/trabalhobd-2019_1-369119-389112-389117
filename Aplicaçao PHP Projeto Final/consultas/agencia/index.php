<?php require_once("../conecta.php"); ?>
<!DOCTYPE html>
<html>
<head>
	<title>Agencia</title>
</head>
<body>
	<h1>Lista Agências</h1>
	<?php 
		$sql = "SELECT * FROM Agencia";
		$result = mysqli_query($con, $sql);

		echo '<a href="https://pdrgms.000webhostapp.com/agencia/form_inserir.php">Inserir Nova Agencia</a>';
		echo "<table>";
		while ($linha = mysqli_fetch_array($result, MYSQLI_ASSOC)){
			echo "<tr>";
			echo "<td>" . $linha["nome"] . "</td>";
			echo "<td>" . $linha["end"] . "</td>";
			if($_SESSION['nivel'] != 'cliente'){
			echo '<td><form method="post" action="form_inserir.php">
					<input type="hidden" name="id" value="'. $linha["idAgencia"] .'">
					<input type="hidden" name="nome" value="'. $linha["nome"] .'">
					<input type="hidden" name="endereco"  value="'. $linha["end"] .'">
					<input type="submit" value="Alterar" id="alterar" name="alterar">
				  </form></td>';
			}
			if($_SESSION['nivel'] != 'cliente'){
				echo '<td><form method="post" action="deleta.php">
						<input type="hidden" name="id" value="'. $linha["idAgencia"] .'">
						<input type="submit" value="Deletar" id="deletar" name="deletar">
					  </form></td>';
			}
			echo "</tr>";
		}
		echo "</table>"; 
	 ?>
</body>
</html>