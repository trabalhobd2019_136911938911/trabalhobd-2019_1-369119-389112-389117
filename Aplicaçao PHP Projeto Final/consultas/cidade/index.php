<?php require_once("../conecta.php"); ?>
<!DOCTYPE html>
<html>
<head>
	<title>Cidade</title>
</head>
<body>
	<h1>Lista de Cidade</h1>
	<?php 
		$sql = "SELECT * FROM Cidade";
		$result = mysqli_query($con, $sql);
		
		echo '<a href="https://pdrgms.000webhostapp.com/cidade/form_inserir.php">Inserir Nova Cidade</a>';
		echo "<table>";
		while ($linha = mysqli_fetch_array($result, MYSQLI_ASSOC)){
			echo "<tr>";
			echo "<td>" . $linha["nome"] . "</td>";
			echo "<td>" . $linha["populacao"] . "</td>";
			echo "<td>" . $linha["estado"] . "</td>";
			if($_SESSION['nivel'] != 'cliente'){
			echo '<td><form method="post" action="form_inserir.php">
					<input type="hidden" name="id" value="'. $linha["idCidade"] .'">
					<input type="hidden" name="nome" value="'. $linha["nome"] .'">
					<input type="hidden" name="populacao"  value="'. $linha["populacao"] .'">
					<input type="hidden" name="estado"  value="'. $linha["estado"] .'">
					<input type="submit" value="Alterar" id="alterar" name="alterar">
				  </form></td>';
			}
			if($_SESSION['nivel'] != 'cliente'){
				echo '<td><form method="post" action="deleta.php">
						<input type="hidden" name="id" value="'. $linha["idCidade"] .'">
						<input type="submit" value="Deletar" id="deletar" name="deletar">
					  </form></td>';
			}
			echo "</tr>";
		}
		echo "</table>"; 
	 ?>
</body>
</html>