<?php require_once("../conecta.php"); ?>
<!DOCTYPE html>
<html>
<head>
	<title>Hotel</title>
</head>
<body>
	<h1>Inserir Hotel</h1>
	<form method="post" action="novo.php">

		<input type="hidden" name="id" value="<?php if(isset($_POST["id"])){ echo $_POST["id"]; } ?>">

		<label>Nome:</label>
		<input type="text" name="nome" value="<?php if(isset($_POST["id"])){ echo $_POST["nome"]; } ?>">

		<label>Categoria:</label>
		<input type="number" name="categoria" value="<?php if(isset($_POST["id"])){ echo $_POST["categoria"]; } ?>">

		<label>Bairro:</label>
		<input type="text" name="bairro" value="<?php if(isset($_POST["id"])){ echo $_POST["bairro"]; } ?>">

		<label>Rua:</label>
		<input type="text" name="rua" value="<?php if(isset($_POST["id"])){ echo $_POST["rua"]; } ?>">
		
		<label>Número:</label>
		<input type="number" name="num" value="<?php if(isset($_POST["id"])){ echo $_POST["num"]; } ?>">
		
		<label>CEP:</label>
		<input type="number" max="99999999"  name="cep" value="<?php if(isset($_POST["id"])){ echo $_POST["cep"]; } ?>">

		<?php
			$sql = "SELECT * FROM `Cidade`";
			$result = mysqli_query($con, $sql);
			echo "<label>Cidade:</label>";
			echo '<select id="cidade" name="cidade">';
			while ($linha = mysqli_fetch_array($result, MYSQLI_ASSOC)){
				if($linha["idCidade"] == $_POST["id"]){
					$selected = 'selected="selected"';
				}
				echo '<option '.$selected.' value="'. $linha["idCidade"] .'" >' . $linha["nome"] . '</option>';
			}
			echo '</select>';
		?>
 
		<input type="submit" value="cadastrar" id="cadastrar" name="Cadastrar">


	</form>

</body>
</html>