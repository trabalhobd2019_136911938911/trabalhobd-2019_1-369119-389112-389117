<?php require_once("../conecta.php"); ?>
<!DOCTYPE html>
<html>
<head>
	<title>Restaurante</title>
</head>
<body>
	<h1>Inserir Restaurante</h1>
	<form method="post" action="novo.php">

		<input type="hidden" name="id" value="<?php if(isset($_POST["id"])){ echo $_POST["id"]; } ?>">

		<label>Nome:</label>
		<input type="text" name="nome" value="<?php if(isset($_POST["nome"])){ echo $_POST["nome"]; } ?>">

		<label>Categoria:</label>
		<input type="text" name="categoria" value="<?php if(isset($_POST["categoria"])){ echo $_POST["categoria"]; } ?>">

		<label>Especialidade:</label>
		<input type="text" name="especialidade" value="<?php if(isset($_POST["especialidade"])){ echo $_POST["especialidade"]; } ?>">

		<label>Preço Médio:</label>
		<input type="number" step="0.01" name="preco" value="<?php if(isset($_POST["preco"])){ echo $_POST["preco"]; } ?>">

		<label>Dia Fechado:</label>
		<select name="dia-fechado">
		  <option <?php if(isset($_POST["diafechado"]) && $_POST["diafechado"] == 'Sunday'){ echo 'selected="selected"'; } ?> value="Sunday">Domingo</option>
		  <option <?php if( isset($_POST["diafechado"]) && $_POST["diafechado"] == 'Monday'){ echo 'selected="selected"'; } ?>  value="Monday">Segunda</option>
		  <option <?php if(isset($_POST["diafechado"]) && $_POST["diafechado"] == 'Tuesday'){ echo 'selected="selected"'; } ?> value="Tuesday">Terça</option>
		  <option <?php if(isset($_POST["diafechado"]) && $_POST["diafechado"] == 'Wednesday'){ echo 'selected="selected"'; } ?> value="Wednesday">Quarta</option>
		  <option <?php if(isset($_POST["diafechado"]) && $_POST["diafechado"] == 'Thursday'){ echo 'selected="selected"'; } ?> value="Thursday">Quinta</option>
		  <option <?php if(isset($_POST["diafechado"]) && $_POST["diafechado"] == 'Friday'){ echo 'selected="selected"'; } ?> value="Friday">Sexta</option>
		  <option <?php if(isset($_POST["diafechado"]) && $_POST["diafechado"] == 'Saturday'){ echo 'selected="selected"'; } ?> value="Saturday">Sábado</option>
		</select>
		
		<label>Bairro:</label>
		<input type="text" name="bairro" value="<?php if(isset($_POST["bairro"])){ echo $_POST["bairro"]; } ?>">

		<label>Rua:</label>
		<input type="text" name="rua" value="<?php if(isset($_POST["rua"])){ echo $_POST["rua"]; } ?>">
		
		<label>Número:</label>
		<input type="number" name="num" value="<?php if(isset($_POST["num"])){ echo $_POST["num"]; } ?>">
		
		<label>CEP:</label>
		<input type="number" name="cep" value="<?php if(isset($_POST["cep"])){ echo $_POST["cep"]; } ?>">

		<?php
			$sql = "SELECT * FROM `Cidade`";
			$result = mysqli_query($con, $sql);
			echo "<label>Cidade:</label>";
			echo '<select id="cidade" name="cidade">';
			while ($linha = mysqli_fetch_array($result, MYSQLI_ASSOC)){
				echo '<option value="' . $linha["idCidade"] . '"">' . $linha["nome"] . '</option>';
			}
			echo '</select>';
		?>

		<?php 
			$sql = "SELECT * FROM `Hotel`";
			$result = mysqli_query($con, $sql);
			echo "<label>Pertence ao Hotel:</label>";
			echo '<select id="hotel" name="hotel">';

			while ($linha = mysqli_fetch_array($result, MYSQLI_ASSOC)){
				if(isset($_POST["hotel"]) && $linha["idHotel"] == $_POST["hotel"]){
					echo '<option selected="selected" value="'. $linha["idHotel"] .'" >' . $linha["nome"] . '</option>';
					$flag = 1;
				}else{
					echo '<option value="'. $linha["idHotel"] .'" >' . $linha["nome"] . '</option>';
				}
				
			}
			if(!isset($flag)){ 
				echo '<option selected="selected" value="" >Nenhum</option>'; 
			}else{
				echo '<option value="">Nenhum</option>';
			}
			echo '</select>';
		?>

		<?php 
			$sql = "SELECT * FROM `Casa_de_show`";
			$result = mysqli_query($con, $sql);
			echo "<label>Pertence a Casa de Show:</label>";
			echo '<select id="casadeshow" name="casadeshow">';
			while ($linha = mysqli_fetch_array($result, MYSQLI_ASSOC)){
				if(isset($_POST["casadeshow"]) && $linha["Ponto_turisticoId"] == $_POST["casadeshow"]){
					echo '<option selected="selected" value="'. $linha["Ponto_turisticoId"] .'" >' . $linha["nome"] . '</option>';
					$flag = 1;
				}else{
					echo '<option value="'. $linha["Ponto_turisticoId"] .'" >' . $linha["nome"] . '</option>';
				}
				
			}
			if(!isset($flag)){ 
				echo '<option selected="selected" value="" >Nenhuma</option>'; 
			}else{
				echo '<option value="">Nenhuma</option>';
			}
			echo '</select>';
		?>


		<input type="submit" value="cadastrar" id="cadastrar" name="Cadastrar">


	</form>

</body>
</html>