<?php require_once("../conecta.php"); ?>
<!DOCTYPE html>
<html>
<head>
	<title>Cidade</title>
</head>
<body>
	<h1>Inserir Cidade</h1>
	<form method="post" action="novo.php">

		<input type="hidden"  name="id" value="<?php if(isset($_POST["id"])){ echo $_POST["id"]; } ?>">

		<label>Nome:</label>
		<input type="text"  name="nome" value="<?php if(isset($_POST["nome"])){ echo $_POST["nome"]; } ?>">

		<label>População:</label>
		<input type="number" name="populacao" value="<?php if(isset($_POST["populacao"])){ echo $_POST["populacao"]; } ?>">

		<label>Estado:</label>
		<input type="text" name="estado" value="<?php if(isset($_POST["estado"])){ echo $_POST["estado"]; } ?>">

		<?php 
			$sql = "SELECT * FROM `Agencia`";
			$result = mysqli_query($con, $sql);
			echo "<label>Agencia:</label>";
			echo '<select id="agencia" name="agencia">';
			while ($linha = mysqli_fetch_array($result, MYSQLI_ASSOC)){
				if($linha["idAgencia"] == $_POST["id"]){
					$selected = 'selected="selected"';
				}
				echo '<option '.$selected.' value="'. $linha["idAgencia"] .'" >' . $linha["nome"] . '</option>';
			}
			echo '</select>';
		?>

		<input type="submit" value="cadastrar" id="cadastrar" name="Cadastrar">


	</form>

</body>
</html>