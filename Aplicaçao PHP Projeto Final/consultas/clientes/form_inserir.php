<?php require_once("../conecta.php"); ?>
<!DOCTYPE html>
<html>
<head>
	<title>Cliente</title>
</head>
<body>
	<h1>Inserir Cliente</h1>
	<form method="post" action="novo.php">

		<input type="hidden" name="id" value="<?php if(isset($_POST["id"])){ echo $_POST["id"]; } ?>">

		<label>Nome:</label>
		<input type="text" name="nome" value="<?php if(isset($_POST["nome"])){ echo $_POST["nome"]; } ?>">

		<label>CPF:</label>
		<input type="number" name="cpf" value="<?php if(isset($_POST["cpf"])){ echo $_POST["cpf"]; } ?>">

		<label>Telefone:</label>
		<input type="number" name="tel" value="<?php if(isset($_POST["tel"])){ echo $_POST["tel"]; } ?>">

		<label>Telefone Opcional:</label>
		<input type="number" name="tel2" value="<?php if(isset($_POST["tel2"])){ echo $_POST["tel2"]; } ?>">

		<?php 
			$sql = "SELECT * FROM `Agencia`";
			$result = mysqli_query($con, $sql);
			echo "<label>Agencia:</label>";
			echo '<select id="agencia" name="agencia">';
			while ($linha = mysqli_fetch_array($result, MYSQLI_ASSOC)){
				if($linha["idAgencia"] == $_POST["id"]){
					$selected = 'selected="selected"';
				}
				echo '<option '.$selected.' value="'. $linha["idAgencia"] .'" >' . $linha["nome"] . '</option>';
			}
			echo '</select>';
		?>

		<input type="submit" value="cadastrar" id="cadastrar" name="Cadastrar">


	</form>

</body>
</html>